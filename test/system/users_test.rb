require "application_system_test_case"

class UsersTest < ApplicationSystemTestCase
  setup do
    @user = {
      email: Faker::Internet.email,
      password: Faker::Internet.password
    }
  end

  test "visiting the index" do
    visit users_url
    assert_selector "h1", text: "Users"
  end

  test "creating a User" do
    visit users_url
    click_on "New User"

    fill_in "user_email", with: @user[:email]
    fill_in "user_password", with: @user[:password]
    click_on "Create User"

    assert_text "Sent an activation email. Please click on the url in the mail to complete the registration."
  end

end
